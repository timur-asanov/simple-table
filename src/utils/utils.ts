import { TableColumn, SortedTableColumn } from "../model/TableColumnTypes";
import { TableSortDirection } from "../model/Constants";

export const countTotalColumns = (columns: Array <TableColumn>, isExpandable: boolean = false): number => {
    let totalColumns = 0;

    if (columns && columns.length > 0) {
        columns.forEach(column => {
            if (column.isHidden === false) {
                totalColumns++;
            }
        });
    }

    if (isExpandable === true) {
        totalColumns += 1;
    }

    return totalColumns;
};

export const getColumnClasses = (tableColumn: TableColumn, totalRows: number, isLoading: boolean): string => {
    const classes: Array<string> = [];

    if (totalRows > 0 && isLoading === false && tableColumn.isSortable === true) {
        classes.push("sortable");
    }

    if (tableColumn.alignRight === true) {
        classes.push("alignRight");
    }

    return classes.join(" ");
};

export const getPageButtonClasses = (currentPage: number, pageNumber: number, totalRows: number, isLoading: boolean): string => {
    const classes: Array<string> = [];

    if (totalRows > 0) {
        if (isLoading === true || pageNumber === currentPage) {
            classes.push("disabled");

            if (pageNumber === currentPage) {
                classes.push("active");
            }
        }
    } else {
        classes.push("disabled");
    }

    return classes.join(" ");
};

export const calculateSortedTableColumn = (currentSortedTableColumn: SortedTableColumn, clickedTableColumn: TableColumn): SortedTableColumn => {
    let newSortedColumn: string | undefined = currentSortedTableColumn.sortedColumn;
    let newSortedDirection: TableSortDirection | undefined = currentSortedTableColumn.sortedDirection;

    if (clickedTableColumn.isSortable === true) {
        if (currentSortedTableColumn.sortedColumn) {
            if (currentSortedTableColumn.sortedColumn === clickedTableColumn.columnKey) {
                if (currentSortedTableColumn.sortedDirection) {
                    if (currentSortedTableColumn.sortedDirection === TableSortDirection.ASC) {
                        newSortedDirection = TableSortDirection.DESC;
                    } else {
                        newSortedColumn = undefined;
                        newSortedDirection = undefined;
                    }
                } else {
                    newSortedDirection = TableSortDirection.ASC;
                }
            } else {
                newSortedColumn = clickedTableColumn.columnKey;
                newSortedDirection = TableSortDirection.ASC;
            }
        } else {
            newSortedColumn = clickedTableColumn.columnKey;
            newSortedDirection = TableSortDirection.ASC;
        }
    }

    return { sortedColumn: newSortedColumn, sortedDirection: newSortedDirection };
};
