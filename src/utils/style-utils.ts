export enum MediaQuerySizes {
    extra_large = 1199,
    large = 991,
    medium = 767,
    small = 575
}

export const mediaQueries = (mediaSize: MediaQuerySizes): (style: TemplateStringsArray | string) => string => {
    return (style: TemplateStringsArray | string) => `@media (max-width: ${mediaSize}px) { ${style} }`;
};
