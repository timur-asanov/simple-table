import { 
    TableColumn, 
    TableColumnOptions, 
    TableDataType, 
    SortedTableColumn, 
    ExtendedRowData, 
    ExtendedRowDataOptions 
} from "./model/TableColumnTypes";
import { DEFAULT_PAGE_SIZE } from "./model/Constants";
import { SimpleTableTheme } from "./model/TableThemeTypes";
import Table from "./components/Table";

export default Table;
export {
    DEFAULT_PAGE_SIZE,
    ExtendedRowData,
    TableColumn
};
export type {
    ExtendedRowDataOptions,
    TableDataType, 
    SimpleTableTheme,
    SortedTableColumn,
    TableColumnOptions
};
