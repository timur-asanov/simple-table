import React, { useState, useEffect, ReactElement, MouseEvent } from 'react';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE_RANGE } from "../model/Constants";
import { getPageButtonClasses } from "../utils/utils";
import { StyledPaginationDiv } from "./table-pagination-styles";

interface TablePaginationInput {
    totalRows: number;
    loading: boolean;

    onPageChange: (pageNumber: number, pageSize: number) => void;
}

const TablePagination = ({totalRows, loading, onPageChange}: TablePaginationInput): ReactElement => {
    const rowsPerPage: number = DEFAULT_PAGE_SIZE;
    const pageRange: number = DEFAULT_PAGE_RANGE;
    const [firstRow, setFirstRow] = useState<number>(0);
    const [currentPage, setCurrentPage] = useState<number>(1);
    const [pages, setPages] = useState<Array<number>>([]);

    useEffect(() => {
        const newPages: Array<number> = new Array<number>();

        if (totalRows > 0) {
            const newTotalPages: number = Math.floor((totalRows / rowsPerPage) + ((totalRows % rowsPerPage !== 0) ? 1 : 0));
            const newPagesLength: number = Math.floor(Math.min(pageRange, newTotalPages));
            let newFirstPage: number = Math.floor(Math.min(Math.max(0, currentPage - (pageRange / 2)), newTotalPages - newPagesLength));

            for (let i = 0; i < newPagesLength; i++) {
                newPages[i] = ++newFirstPage;
            }
        } else {
            newPages[0] = 1;
        }

        setPages(newPages);
    }, [totalRows, currentPage, rowsPerPage, pageRange]);

    const pageFirst = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        page(0);
    };

    const pageNext = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        page(firstRow + rowsPerPage);
    };

    const pagePrevious = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        page(firstRow - rowsPerPage);
    };

    const pageLast = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        page(totalRows - ((totalRows % rowsPerPage !== 0) ? totalRows % rowsPerPage : rowsPerPage));
    };

    const pageLink = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        const pageNumber: number = event.currentTarget.value as unknown as number;

        page((pageNumber - 1) * rowsPerPage);
    };

    const page = (newFirstRow: number) => {
        const newCurrentPage: number = Math.floor((totalRows / rowsPerPage) - ((totalRows - newFirstRow) / rowsPerPage) + 1);

        setFirstRow(newFirstRow);
        setCurrentPage(newCurrentPage);

        onPageChange(newCurrentPage, rowsPerPage);
    };

    return (
        <StyledPaginationDiv>
            <ul>
                <li className={loading === true || firstRow === 0 ? "disabled" : ""}>
                    <button onClick={pageFirst} disabled={loading === true || firstRow === 0}>First</button>
                </li>
                <li className={loading === true || firstRow === 0 ? "disabled" : ""}>
                    <button onClick={pagePrevious} disabled={loading === true || firstRow === 0}>Previous</button>
                </li>
                {pages.map((pageNumber, index) => {
                     return (
                        <li key={index} className={getPageButtonClasses(currentPage, pageNumber, totalRows, loading)}>
                            <button onClick={pageLink} value={pageNumber} disabled={loading === true || pageNumber === currentPage}>{pageNumber}</button>
                        </li>
                    );
                })}
                <li className={loading === true || ((firstRow + rowsPerPage) >= totalRows) ? "disabled" : ""}>
                    <button onClick={pageNext} disabled={loading === true || ((firstRow + rowsPerPage) >= totalRows)}>Next</button>
                </li>
                <li className={loading === true || ((firstRow + rowsPerPage) >= totalRows) ? "disabled" : ""}>
                    <button onClick={pageLast} disabled={loading === true || ((firstRow + rowsPerPage) >= totalRows)}>Last</button>
                </li>
            </ul>
        </StyledPaginationDiv>
    );
};

export default TablePagination;
