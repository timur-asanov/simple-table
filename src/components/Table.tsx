import React, { ReactElement } from 'react';
import { ThemeProvider } from "styled-components";
import { TableColumn, SortedTableColumn, TableDataType } from "../model/TableColumnTypes";
import TableHeader from "./TableHeader";
import TableRow from "./rows/TableRow";
import TableRowNoData from "./rows/TableRowNoData";
import TableRowLoading from "./rows/TableRowLoading";
import TablePagination from "./TablePagination";
import { countTotalColumns } from "../utils/utils";
import { SimpleTableTheme, DEFAULT_THEME } from "../model/TableThemeTypes";
import { StyledTable } from "./table-styles";

interface TableInput {
    columns: Array<TableColumn>;
    tableData: Array<TableDataType>;
    totalRows: number;
    theme?: SimpleTableTheme;
    expandableRows?: boolean;
    loading?: boolean;

    onPageChange: (pageNumber: number, pageSize: number) => void;
    onSortChange?: (sortedTableColumn: SortedTableColumn) => void;
}

/*
    validation
    - if columns && columns.length > 0
    
    additional options
    - on sort, reset page to 1: true | false - current: false
    - show pagination if there is only one page: true | false - current: true
    - page size change: [int, int, int, ...] + onPageSizeChange
 */
const Table: React.FC <TableInput>  = ({
    columns, 
    tableData, 
    totalRows, 
    onPageChange, 
    onSortChange, 
    theme, 
    expandableRows = false, 
    loading = false
}: TableInput): ReactElement => {
    let tableTheme: SimpleTableTheme = Object.assign({}, DEFAULT_THEME);

    if (theme) {
        tableTheme = Object.assign({}, tableTheme, theme);
    }

    const totalColumns = countTotalColumns(columns, expandableRows);

    return (
        <ThemeProvider theme={tableTheme}>
            <div className={"st"}>
                <StyledTable>
                    <TableHeader 
                        columns={columns} 
                        totalRows={totalRows} 
                        loading={loading} 
                        onSortChange={onSortChange} 
                        isExpandable={expandableRows} 
                    />
                    <tbody>
                        {loading === true ? 
                            <TableRowLoading totalColumns={totalColumns} />
                            : 
                            tableData && tableData.length > 0 ? 
                                tableData.map(tableDataRow => {
                                    return (
                                        <TableRow 
                                            key={tableDataRow.id} 
                                            columns={columns} 
                                            totalColumns={totalColumns} 
                                            tableDataRow={tableDataRow} 
                                            isExpandable={expandableRows} 
                                        />
                                    );
                                })
                            :
                            <TableRowNoData totalColumns={totalColumns} />
                        }
                    </tbody>
                </StyledTable>
                <TablePagination totalRows={totalRows} loading={loading} onPageChange={onPageChange} />
            </div>
        </ThemeProvider>
    );
};

export default Table;
