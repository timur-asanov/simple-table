import styled from "styled-components";
import cssesc from "cssesc";

export const StyledTableRow = styled.tr`
    font-size: ${props => cssesc(props.theme.tableRow.fontSize)};
    
    td {
        border: ${props => {
            return (
                cssesc(props.theme.tableRow.borderWidth) + " " 
                + cssesc(props.theme.tableRow.borderType) + " " 
                + cssesc(props.theme.tableRow.colors.border)
            );
        }};
        text-align: ${props => props.theme.tableRow.alignCenter === true ? "center" : "left" };
        padding: ${props => cssesc(props.theme.tableRow.padding)};
    }

    td > div {
        text-overflow: ${props => cssesc(props.theme.tableRow.textOverflow)};
        overflow: hidden;
        white-space: nowrap;
    }

    td.alignRight {
        text-align: right;
    }

    td.actionColumn {
        text-align: center;
        width: ${props => cssesc(props.theme.tableHeader.actionColumnWidth)};
    }

    td button {
        margin: 0;
        padding: 0;
        border: none;
        background: none;
        cursor: pointer;
    }
`;
