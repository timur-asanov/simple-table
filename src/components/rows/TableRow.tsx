import React, { useState, MouseEvent, ReactElement } from 'react';
import TableRowExtended from "./TableRowExtended";
import { TableColumn, TableDataType } from "../../model/TableColumnTypes";
import { StyledTableRow } from "./table-row-styles";

interface TableRowInput {
    columns: Array<TableColumn>;
    tableDataRow: TableDataType;
    totalColumns: number;
    isExpandable?: boolean;
}

const TableRow = ({columns, tableDataRow, totalColumns, isExpandable = false}: TableRowInput): ReactElement => {
    const [areDetailsShown, setDetailsShown] = useState<boolean>(false);

    const toggleDetails = (event: MouseEvent<HTMLButtonElement>): void => {
        event.preventDefault();
        setDetailsShown(!areDetailsShown);
    };

    return (
        <React.Fragment>
            <StyledTableRow>
                {isExpandable === true && <td className={"actionColumn"}><button onClick={toggleDetails}><i className={'fa ' + (areDetailsShown === true ? 'fa-chevron-down' : 'fa-chevron-right')}></i></button></td>}
                {columns && columns.filter(tableColumn => tableColumn.isHidden === false).map((tableColumn, index) => {
                    return (
                        <td key={index} className={tableColumn.alignRight === true ? "alignRight" : ''}>
                            <div>{tableDataRow.data[tableColumn.columnKey]}</div>
                        </td>);
                })}
            </StyledTableRow>
            {isExpandable === true && areDetailsShown === true && 
                <TableRowExtended 
                    colSpan={totalColumns}
                    tableDataRow={tableDataRow}
                />
            }
        </React.Fragment>
    );
};

export default TableRow;
