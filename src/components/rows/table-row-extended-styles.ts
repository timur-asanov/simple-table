import styled from "styled-components";
import cssesc from "cssesc";

export const StyledTableRowExtended = styled.tr`
    td {
        border: ${props => {
            return (
                cssesc(props.theme.tableRow.borderWidth) + " " 
                + cssesc(props.theme.tableRow.borderType) + " " 
                + cssesc(props.theme.tableRow.colors.border)
            );
        }};
        text-align: left;
        padding: ${props => cssesc(props.theme.tableRowExtended.padding)}
    }

    td.contentRow {
        padding-bottom: 0;
        padding-top: 0;
    }

    td.contentRow .container {
        min-height: 0;
        height: auto;
        overflow: visible;
        transition: height 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
        transition-duration: 266ms;
    }

    td.contentRow .container .wrapper {
        display: inline-block;
        width: 100%;
        margin: ${props => cssesc(props.theme.tableRowExtended.margin)};
    }

    td.contentRow .container .wrapper h6 {
        clear: both;
        float: left;
        padding: 0;
        margin: 0;
        font-size: ${props => cssesc(props.theme.tableRowExtended.headerFontSize)};
        font-weight: ${props => cssesc(props.theme.tableRowExtended.headerFontWeight)};
    }

    td.contentRow .container .wrapper p {
        clear: both;
        float: left;
        padding: 0;
        margin: 10px 0 0 0;
        font-size: ${props => cssesc(props.theme.tableRowExtended.fontSize)};
    }
`;
