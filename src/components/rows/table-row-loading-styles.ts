import styled from "styled-components";
import cssesc from "cssesc";

export const StyledTableRow = styled.tr`
    td {
        border: ${props => {
            return (
                cssesc(props.theme.tableRow.borderWidth) + " " 
                + cssesc(props.theme.tableRow.borderType) + " " 
                + cssesc(props.theme.tableRow.colors.border)
            );
        }};
    }

    .loading-spinner {
        border: ${props => {
            return (
                cssesc(props.theme.loadingSpinner.width) 
                + " solid " 
                + cssesc(props.theme.loadingSpinner.colors.backgroundColor)
            );
        }};
        border-top: ${props => {
            return (
                cssesc(props.theme.loadingSpinner.width) 
                + " solid " 
                + cssesc(props.theme.loadingSpinner.colors.segmentColor)
            );
        }};
        border-radius: 50%;
        width: ${props => cssesc(props.theme.loadingSpinner.size)};
        height: ${props => cssesc(props.theme.loadingSpinner.size)};
        animation: loading-spinner 2s linear infinite;
        margin: 10px auto;
    }

    @keyframes loading-spinner {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
`;
