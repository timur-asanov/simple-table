import React, { ReactElement } from 'react';
import { StyledTableRow } from "./table-row-loading-styles";

interface TableRowLoadingInput {
    totalColumns: number;
}

const TableRowLoading = ({totalColumns}: TableRowLoadingInput): ReactElement => {
    return (
        <StyledTableRow>
            <td colSpan={totalColumns}>
                <div className={"loading-spinner"}></div>
            </td>
        </StyledTableRow>
    );
};

export default TableRowLoading;
