import React, { ReactElement } from 'react';
import DOMPurify from 'dompurify';
import { TableDataType } from "../../model/TableColumnTypes";
import { StyledTableRowExtended } from "./table-row-extended-styles";

interface TableRowExtendedInput {
    colSpan: number;
    tableDataRow: TableDataType;
}

const TableRowExtended = ({colSpan, tableDataRow}: TableRowExtendedInput): ReactElement => {
    return (tableDataRow.rowDetails ? 
        <StyledTableRowExtended>
            <td colSpan={colSpan} className={"contentRow"}>
                <div className={"container"}>
                    <div className={"wrapper"}>
                        <h6>{tableDataRow.rowDetails.headerText}</h6>
                        {tableDataRow.rowDetails.description && 
                            <p dangerouslySetInnerHTML={{__html: DOMPurify.sanitize(tableDataRow.rowDetails.description)}}></p>
                        }
                    </div>
                </div>
            </td>
        </StyledTableRowExtended>
        :
        <></>
    );
};

export default TableRowExtended;
