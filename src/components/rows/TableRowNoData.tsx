import React, { ReactElement } from 'react';
import { TableMessages } from "../../model/Constants";
import { StyledTableRow } from "./table-row-no-data-styles";

interface TableRowNoDataInput {
    totalColumns: number;
}

const TableRowNoData = ({totalColumns}: TableRowNoDataInput): ReactElement => {
    return (
        <StyledTableRow>
            <td colSpan={totalColumns}>
                <div>{TableMessages.NO_DATA}</div>
            </td>
        </StyledTableRow>
    );
};

export default TableRowNoData;
