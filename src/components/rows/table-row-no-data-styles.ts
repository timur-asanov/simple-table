import styled from "styled-components";
import cssesc from "cssesc";

export const StyledTableRow = styled.tr`
    font-size: ${props => cssesc(props.theme.tableRow.fontSize)};
    
    td {
        border: ${props => {
            return (
                cssesc(props.theme.tableRow.borderWidth) + " " 
                + cssesc(props.theme.tableRow.borderType) + " " 
                + cssesc(props.theme.tableRow.colors.border)
            );
        }};
        text-align: center;
    }

    td > div {
        margin: 10px;
    }
`;
