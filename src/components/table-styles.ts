import styled from "styled-components";
import cssesc from "cssesc";
import { mediaQueries, MediaQuerySizes } from "../utils/style-utils";

export const StyledTable = styled.table`
    border-spacing: 0;
    border-collapse: collapse;
    width: 100%;
    table-layout: fixed;
    font-size: ${props => cssesc(props.theme.fontSize)};

    /* background-color: grey; */

    ${mediaQueries(MediaQuerySizes.extra_large)`
        /* background-color: red; */
    `};

    ${mediaQueries(MediaQuerySizes.large)`
        /* background-color: green; */
    `};

    ${mediaQueries(MediaQuerySizes.medium)`
        /* background-color: yellow; */
    `};

    ${mediaQueries(MediaQuerySizes.small)`
        /* background-color: blue; */
    `};
`;
