import styled from "styled-components";
import cssesc from "cssesc";

export const StyledTableHeader = styled.thead`
    display: table-header-group;
    font-size: ${props => cssesc(props.theme.tableHeader.fontSize)};

    th {
        border: ${props => {
            return (
                cssesc(props.theme.tableHeader.borderWidth) + " " 
                + cssesc(props.theme.tableHeader.borderType) + " " 
                + cssesc(props.theme.tableHeader.colors.border)
            );
        }};
        text-align: ${props => props.theme.tableHeader.alignCenter === true ? "center" : "left" };
        padding: ${props => cssesc(props.theme.tableHeader.padding)};
        font-weight: ${props => cssesc(props.theme.tableHeader.fontWeight)};
    }

    th.alignRight {
        text-align: right;
    }

    th.actionColumn {
        text-align: center;
        width: ${props => cssesc(props.theme.tableHeader.actionColumnWidth)};
    }

    th.sortable {
        cursor: pointer;
    }
`;
