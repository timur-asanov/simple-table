import React, { ReactElement, useState } from 'react';
import { getColumnClasses, calculateSortedTableColumn } from "../utils/utils";
import { TableColumn, SortedTableColumn } from "../model/TableColumnTypes";
import { TableSortDirection } from "../model/Constants";
import { StyledTableHeader } from "./table-header-styles";

interface TableHeaderInput {
    columns: Array<TableColumn>;
    totalRows: number;
    loading: boolean;
    isExpandable?: boolean;

    onSortChange?: (sortedTableColumn: SortedTableColumn) => void;
}

const TableHeader = ({columns, totalRows, onSortChange, loading, isExpandable = false}: TableHeaderInput): ReactElement => {
    const [sortedTableColumn, setSortedTableColumn] = useState<SortedTableColumn>({ sortedColumn: undefined, sortedDirection: undefined });

    const showSortArrow = (tableColumn: TableColumn): ReactElement => {
        if (sortedTableColumn.sortedColumn && sortedTableColumn.sortedDirection && tableColumn.isSortable === true && sortedTableColumn.sortedColumn === tableColumn.columnKey) {
            return <i className={'fa ' + (sortedTableColumn.sortedDirection === TableSortDirection.ASC ? 'fa-chevron-down' : 'fa-chevron-up')}></i>;
        }

        return <></>;
    };

    const onColumnHeaderClicked = (tableColumn: TableColumn): void => {
        if (tableColumn.isSortable === true && onSortChange) {
            const newSortedTableColumn: SortedTableColumn = calculateSortedTableColumn(sortedTableColumn, tableColumn);

            setSortedTableColumn(newSortedTableColumn);
            onSortChange(newSortedTableColumn);
        }
    };

    return (
        <StyledTableHeader>
            <tr>
                {isExpandable === true && <th className={"actionColumn"}></th>}
                {columns && columns.filter(tableColumn => tableColumn.isHidden === false).map((tableColumn, index) => {
                    return (
                        <th 
                            key={index} 
                            className={getColumnClasses(tableColumn, totalRows, loading)} 
                            onClick={() => {
                                if (totalRows > 0 && loading === false && tableColumn.isSortable === true && onSortChange) {
                                    onColumnHeaderClicked(tableColumn);
                                }
                            }}>

                            {tableColumn.headerText}{showSortArrow(tableColumn)}
                        </th>
                    );
                })}
            </tr>
        </StyledTableHeader>
    );
};

export default TableHeader;
