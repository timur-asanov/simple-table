import styled from "styled-components";
import cssesc from "cssesc";

export const StyledPaginationDiv = styled.div`
    clear: both;
    float: ${props => cssesc(props.theme.tablePagination.float)};
    margin: ${props => cssesc(props.theme.tablePagination.margin)};
    border-radius: ${props => cssesc(props.theme.tablePagination.borderRadius)};
    font-size: ${props => cssesc(props.theme.tablePagination.fontSize)};

    ul {
        display: inline-block;
        margin: 0;
        padding: 0;
        border-radius: ${props => cssesc(props.theme.tablePagination.borderRadius)};
        box-shadow: ${props => cssesc(props.theme.tablePagination.boxShadow)};
    }

    ul > li {
        display: inline;
    }

    ul > li > button {
        float: left;
        padding: ${props => cssesc(props.theme.tablePagination.button.padding)};
        text-decoration: ${props => cssesc(props.theme.tablePagination.button.textDecoration)};
        background-color: ${props => cssesc(props.theme.tablePagination.button.colors.backgroundColor)};
        border: ${props => {
            return (
                cssesc(props.theme.tablePagination.borderWidth) + " " 
                + cssesc(props.theme.tablePagination.borderType) + " " 
                + cssesc(props.theme.tablePagination.colors.border)
            );
        }};
        border-left-width: 0;
        cursor: pointer;
    }

    ul > li > button:hover {
        text-decoration: ${props => cssesc(props.theme.tablePagination.button.hover.textDecoration)};
    }

    ul > li:first-child > button {
        border-left-width: ${props => cssesc(props.theme.tablePagination.borderWidth)};
        border-top-left-radius: ${props => cssesc(props.theme.tablePagination.borderRadius)};
        border-bottom-left-radius: ${props => cssesc(props.theme.tablePagination.borderRadius)};
    }

    ul > li:last-child > button {
        border-right-width: ${props => cssesc(props.theme.tablePagination.borderWidth)};
        border-top-right-radius: ${props => cssesc(props.theme.tablePagination.borderRadius)};
        border-bottom-right-radius: ${props => cssesc(props.theme.tablePagination.borderRadius)};
    }

    ul > li.disabled > button,
    ul > li.disabled > button:hover,
    ul > li.disabled > button:focus {
        color: ${props => cssesc(props.theme.tablePagination.button.colors.disabledTextColor)};
        cursor: default;
        text-decoration: ${props => cssesc(props.theme.tablePagination.button.disabled.textDecoration)};
    }

    ul > li.active > button,
    ul > li.active > button:hover,
    ul > li.active > button:focus {
        background-color: ${props => cssesc(props.theme.tablePagination.button.colors.activeBackgroundColor)};
        color: ${props => cssesc(props.theme.tablePagination.button.colors.activeTextColor)};
    }
`;
