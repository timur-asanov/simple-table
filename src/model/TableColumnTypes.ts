import { TableSortDirection } from "./Constants";

export interface TableDataType {
    id: string;
    data: {[key: string]: number | string};
    rowDetails?: ExtendedRowData;
}

export interface TableColumnOptions {
    columnKey: string;
    headerText: string;
    alignRight?: boolean;
    sortable?: boolean;
    hidden?: boolean;
}

export interface SortedTableColumn {
    sortedColumn: string | undefined;
    sortedDirection: TableSortDirection | undefined;
}

export class TableColumn {
    private _columnKey: string;
    private _headerText: string;
    private _alignRight: boolean;
    private _isSortable: boolean;
    private _isHidden: boolean;

    constructor(options: TableColumnOptions) {
        this._columnKey = options.columnKey;
        this._headerText = options.headerText;
        this._alignRight = options.alignRight === true;
        this._isSortable = options.sortable === true;
        this._isHidden = options.hidden === true;

        if (options.columnKey === "id") {
            this._isHidden = true;
        }
    }

    get columnKey(): string {
        return this._columnKey;
    }

    get headerText(): string {
        return this._headerText;
    }

    get alignRight(): boolean {
        return this._alignRight;
    }

    get isSortable(): boolean {
        return this._isSortable;
    }

    get isHidden(): boolean {
        return this._isHidden;
    }
}

export interface ExtendedRowDataOptions {
    headerText: string;
    description?: string | undefined;
}

export class ExtendedRowData {
    private _headerText: string;
    private _description: string | undefined;

    constructor(options: ExtendedRowDataOptions) {
        this._headerText = options.headerText;
        this._description = options.description;
    }

    get headerText(): string {
        return this._headerText;
    }

    get description(): string | undefined {
        return this._description;
    }
}
