import { DefaultTheme } from "styled-components";

export interface SimpleTableTheme extends DefaultTheme {
    fontSize?: string;

    colors?: {
        
    };

    tableHeader?: {
        actionColumnWidth?: string;
        alignCenter?: boolean;
        borderType?: string;
        borderWidth?: string;
        fontSize?: string;
        fontWeight?: string;
        padding?: string;

        colors?: {
            border?: string;
        };
    };

    tableRow?: {
        alignCenter?: boolean;
        borderType?: string;
        borderWidth?: string;
        fontSize?: string;
        padding?: string;
        textOverflow?: string;

        colors?: {
            border?: string;
        };
    };

    tableRowExtended?: {
        fontSize?: string;
        headerFontSize?: string;
        headerFontWeight?: string;
        margin?: string;
        padding?: string;
    };

    loadingSpinner?: {
        width?: string;
        size?: string;

        colors?: {
            backgroundColor?: string;
            segmentColor?: string;
        };
    };

    tablePagination?: {
        borderRadius?: string;
        borderType?: string;
        borderWidth?: string;
        boxShadow?: string;
        buttonPadding?: string;
        float?: string;
        fontSize?: string;
        margin?: string;

        button?: {
            padding?: string;
            textDecoration?: string;

            hover?: {
                textDecoration?: string;
            }

            disabled?: {
                textDecoration?: string;
            }

            active?: {
                
            }

            colors?: {
                backgroundColor?: string;
                disabledTextColor?: string;
                activeBackgroundColor?: string;
                activeTextColor?: string;
            };
        }

        colors?: {
            border?: string;
        };
    };
}

export const DEFAULT_THEME: SimpleTableTheme = {
    fontSize: "inherit",

    colors: {
        
    },

    tableHeader: {
        actionColumnWidth: "30px",
        alignCenter: false,
        borderType: "solid",
        borderWidth: "1px",
        fontSize: "inherit",
        fontWeight: "bold",
        padding: "3px 10px",

        colors: {
            border: "#ddd"
        }
    },

    tableRow: {
        alignCenter: false,
        borderType: "solid",
        borderWidth: "1px",
        fontSize: "inherit",
        padding: "3px 10px",
        textOverflow: "ellipsis",

        colors: {
            border: "#ddd"
        }
    },

    tableRowExtended: {
        fontSize: "inherit",
        headerFontSize: "inherit",
        headerFontWeight: "normal",
        margin: "8px 0",
        padding: "3px 10px",
    },

    loadingSpinner: {
        width: "2px",
        size: "20px",

        colors: {
            backgroundColor: "#ddd",
            segmentColor: "#000"
        }
    },

    tablePagination: {
        borderRadius: "4px",
        borderType: "solid",
        borderWidth: "1px",
        boxShadow: "0 1px 2px rgba(0, 0, 0, 0.05)",
        float: "right",
        fontSize: "inherit",
        margin: "20px 0 0 0",

        button: {
            padding: "4px 12px",
            textDecoration: "none",

            hover: {
                textDecoration: "underline",
            },

            disabled: {
                textDecoration: "none",
            },

            active: {
                
            },

            colors: {
                backgroundColor: "#fff",
                disabledTextColor: "#999",
                activeBackgroundColor: "#ddd",
                activeTextColor: "#000"
            }
        },

        colors: {
            border: "#ddd"
        }
    }
};
