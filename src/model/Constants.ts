export const DEFAULT_PAGE_SIZE: number = 5;
export const DEFAULT_PAGE_RANGE: number = 5;

export enum TableSortDirection {
    ASC = "asc",
    DESC = "desc"
}

export enum TableMessages {
    NO_DATA = "no data to display.."
}
