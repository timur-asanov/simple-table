import React, { useState, useEffect, ReactElement } from 'react';
import ReactDOM from 'react-dom';
import { v4 as uuidv4 } from 'uuid';
import Table, { 
    ExtendedRowData,
    TableColumn, 
    TableDataType, 
    SortedTableColumn, 
    DEFAULT_PAGE_SIZE 
} from "@npmtima-org/simple-table";

import 'font-awesome/css/font-awesome.min.css';

const LONG_DESC: string = "Et aut quia minima qui qui blanditiis. Et dolores neque vel. Sed ut repellendus omnis quod. " 
    + "Iusto voluptatem autem rerum quis veniam blanditiis voluptas ab. Omnis placeat quaerat doloremque velit explicabo " 
    + "aliquid perferendis dolores.<br><br>Et voluptate ducimus ea nihil. Sunt impedit recusandae quo qui voluptas " 
    + "dolores adipisci. Labore amet ab eos sint. Consequatur accusamus ea quos voluptatibus et. Voluptatem rerum qui ut " 
    + "consequatur quis.<br /><br />Modi reprehenderit officiis et molestiae. Qui veritatis et dolor sed nihil repellendus. " 
    + "Deserunt qui rerum officia esse. Ad officiis cupiditate illo ullam sed voluptates quod optio. Earum deleniti " 
    + "molestiae provident. Et repellendus occaecati exercitationem velit hic non et quaerat.<br /><br />Dolores qui " 
    + "ratione iste libero architecto qui omnis officiis. Sed neque in et non labore suscipit inventore. Ea enim " + 
    "voluptatibus et minima. Unde est et magni tempora dolores earum doloribus.<br /><br />Consequatur in tempora " 
    + "deserunt perferendis perspiciatis laboriosam. Est minus consequuntur quam blanditiis quisquam. Atque ut quia et " 
    + "minus quis. Numquam earum omnis incidunt impedit eum quia architecto nemo. Eligendi et aut ab a. Odio ratione " 
    + "quis pariatur recusandae veniam quam.";

const SHORT_DESC: string = "Et aut quia minima qui qui blanditiis. Et dolores neque vel. Sed ut repellendus omnis quod. " 
    + "Iusto voluptatem autem rerum quis veniam blanditiis voluptas ab. Omnis placeat quaerat doloremque velit explicabo " 
    + "aliquid perferendis dolores.Et voluptate ducimus ea nihil. Sunt impedit recusandae quo qui voluptas " 
    + "dolores adipisci. Labore amet ab eos sint. Consequatur accusamus ea quos voluptatibus et. Voluptatem rerum qui ut " 
    + "consequatur quis.Modi reprehenderit officiis et molestiae. Qui veritatis et dolor sed nihil repellendus. " 
    + "Deserunt qui rerum officia esse. Ad officiis cupiditate illo ullam sed voluptates quod optio. Earum deleniti " 
    + "molestiae provident. Et repellendus occaecati exercitationem velit hic non et quaerat.";

const populateArray = (itemsCount: number): Array<TableDataType> => {
    const array: Array<TableDataType> = [];

    for (let i = 0; i < itemsCount; i++) {
        const id: string = uuidv4();
        const description: string | undefined = (i % 4 === 1 ? LONG_DESC : (i % 4 === 2 ? SHORT_DESC : undefined));

        array.push({
            id: id,
            data: {
                column_1: "column_1_" + i + (i % 4 === 1 ? "_01234567891011121314151617181920" : ""),
                column_2: "column_2_" + i,
                column_3: "column_3_" + i + (i % 2 === 0 ? "_01234 5678910111 2131415161 7181920" : ""),
                column_4: i,
                column_5: i,
            },
            rowDetails: new ExtendedRowData({headerText: id, description: description})
        });
    }

    return array;
};

const shuffleArray = (array: Array<TableDataType>): void => {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));

        [array[i], array[j]] = [array[j], array[i]];
    }
};

const columns: Array<TableColumn> = [];
columns.push(new TableColumn({columnKey: "id", headerText: "ID"}));
columns.push(new TableColumn({columnKey: "column_1", headerText: "Column 1 (long long very long)"}));
columns.push(new TableColumn({columnKey: "column_2", headerText: "Column 2"}));
columns.push(new TableColumn({columnKey: "column_3", headerText: "Column 3", alignRight: true}));
columns.push(new TableColumn({columnKey: "column_4", headerText: "Column 4", alignRight: true, sortable: true}));
columns.push(new TableColumn({columnKey: "column_5", headerText: "Column 5 (hidden)", hidden: true}));

const allData: Array<TableDataType> = populateArray(50);
shuffleArray(allData);

const App: React.FC = (): ReactElement => {
    const [tableData, setTableData] = useState<Array<TableDataType>>([]);
    const [pageNumber, setPageNumber] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(DEFAULT_PAGE_SIZE);
    const [sortedTableColumn, setSortedTableColumn] = useState<SortedTableColumn>({ sortedColumn: undefined, sortedDirection: undefined });

    useEffect(() => {
        let sortedData: Array<TableDataType> = [...allData];

        if (sortedTableColumn.sortedColumn && sortedTableColumn.sortedDirection) {
            const tableColumn: TableColumn | undefined = columns.find(column => column.columnKey === sortedTableColumn.sortedColumn);

            if (tableColumn) {
                sortedData = sortedData.sort((a, b): number => {
                    /*eslint-disable @typescript-eslint/no-explicit-any */
                    const value1: any = sortedTableColumn.sortedColumn && a.data[sortedTableColumn.sortedColumn];
                    const value2: any = sortedTableColumn.sortedColumn && b.data[sortedTableColumn.sortedColumn];
                    /*eslint-enable @typescript-eslint/no-explicit-any */

                    if (typeof value1 === "string") {
                        return value1 < value2 ? -1 : value1 > value2 ? 1 : 0;
                    } else if (typeof value1 === "number") {
                        return value1 - value2;
                    }

                    return 0;
                });

                if (sortedTableColumn.sortedDirection === "asc") {
                    sortedData.reverse();
                }
            }
        }

        const newTableData: Array<TableDataType> = sortedData.slice((pageNumber - 1) * pageSize, pageNumber * pageSize);

        setTableData(newTableData);
    }, [pageNumber, pageSize, sortedTableColumn]);

    const onPageChange = (pageNumber: number, pageSize: number): void => {
        setPageNumber(pageNumber);
        setPageSize(pageSize);
    };

    const onSortChange = (sortedTableColumn: SortedTableColumn): void => {
        setSortedTableColumn(sortedTableColumn);
    };

    return (
        <div>
            <h3>Simple Table Testing App</h3>
            <Table 
                columns={columns} 
                tableData={tableData} 
                totalRows={allData.length} 
                onPageChange={onPageChange} 
                onSortChange={onSortChange} 
                expandableRows={true}
            />
            <Table 
                columns={columns} 
                tableData={[]} 
                totalRows={0} 
                onPageChange={onPageChange} 
                onSortChange={onSortChange} 
                expandableRows={true}
            />
            <Table 
                columns={columns} 
                tableData={tableData} 
                totalRows={allData.length} 
                onPageChange={onPageChange} 
                onSortChange={onSortChange} 
                loading={true}
            />
            <Table 
                columns={columns} 
                tableData={[]} 
                totalRows={0} 
                onPageChange={onPageChange} 
                onSortChange={onSortChange} 
                expandableRows={true} 
                loading={true}
            />
        </div>
    );
};

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root')
);
