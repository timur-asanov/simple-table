export default {
    roots: [
        "<rootDir>/src"
    ],
    clearMocks: true,
    coverageProvider: "babel",
    coverageReporters: [
        "json",
        "text"
    ],
    collectCoverageFrom: [
        "src/**/*.{ts,tsx}",
        "!src/**/*.d.ts"
    ],
    setupFiles: [
        "react-app-polyfill/jsdom"
    ],
    setupFilesAfterEnv: [
        "<rootDir>/setupTests.ts"
    ],
    testMatch: [
        "<rootDir>/src/__tests__/**/*.{ts,tsx}"
    ],
    testEnvironment: "jsdom",
    transform: {
        "^.+\\.(ts|tsx)$": "babel-jest"
    },
    transformIgnorePatterns: [
        "[/\\\\]node_modules[/\\\\].+\\.(js|jsx|mjs|cjs|ts|tsx)$",
        "^.+\\.module\\.(css|less)$"
    ],
    modulePaths: [],
    moduleNameMapper: {
        "^.+\\.module\\.(css|less)$": "identity-obj-proxy"
    },
    moduleFileExtensions: [
        "web.js",
        "js",
        "web.ts",
        "ts",
        "web.tsx",
        "tsx",
        "json",
        "web.jsx",
        "jsx",
        "node"
    ],
    watchPlugins: [
        "jest-watch-typeahead/filename",
        "jest-watch-typeahead/testname"
    ]
};
